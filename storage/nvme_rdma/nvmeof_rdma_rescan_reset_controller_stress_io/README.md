# storage/nvme_rdma/nvmeof_rdma_rescan_reset_controller_stress_io

Storage: nvmeof rdma rescan/reset controller stress during io

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
